;
; BIND data file for local loopback interface
;
$TTL	604800
@	IN	SOA	servercs.bizzbizzticket.com. root.bizzbizzticket.com. (
			      2		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
@	IN	NS	servercs.bizzbizzticket.com.
@	IN	MX	10	mail.bizzbizzticket.com.
servercs.bizzbizzticket.com.	IN	A	192.168.9.1
ale.bizzbizzticket.com.	IN	A	192.168.9.2
sara.bizzbizzticket.com.	IN	A	192.168.9.3
www	IN	CNAME	servercs.bizzbizzticket.com.
ftp	IN	CNAME	servercs.bizzbizzticket.com.
mail	IN	CNAME	servercs.bizzbizzticket.com.
